package com.example.task;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

public class FadeOutActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fade_out_activty);

        imageView = findViewById(R.id.fade_out_img);
        AlphaAnimation animation = new AlphaAnimation(1, (float) 0.0);
        animation.setFillAfter(true);
        animation.setDuration(4000);
        imageView.startAnimation(animation);
    }
}
