package com.example.task;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    LinearLayout parent, linear_layout;
    TextView txtDes, txtTitle;
    Intent intent;
    PopupWindow popupWindow;
    View customView;
    Button btnCancel, btnSave;
    EditText editDes, editTitle;
    ActionBar menu;


    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);


        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {


        if (item.getItemId() == R.id.btn_add) {


            showPopup();


            btnCancel = customView.findViewById(R.id.btn_cancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menu.show();
                    popupWindow.dismiss();
                }
            });


            btnSave = customView.findViewById(R.id.btn_save);

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menu.show();
                    save();

                }
            });


        }
        if (item.getItemId() == R.id.clear) {
            parent.removeAllViews();
        }

        if (item.getItemId() == R.id.fade_in) {
            intent = new Intent(getApplicationContext(), FadeInActivity.class);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.fade_out) {
            intent = new Intent(getApplicationContext(), FadeOutActivity.class);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.zoom) {
            intent = new Intent(getApplicationContext(), ZoomActivity.class);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.rotate) {
            intent = new Intent(getApplicationContext(), RotateActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void showPopup() {
        menu = getSupportActionBar();

        menu.hide();
        inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        customView = inflater.inflate(R.layout.pop_up_activity, null);
        popupWindow = new PopupWindow(customView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setTouchModal(false);
        popupWindow.setAnimationStyle(R.style.slide_in_out);
        popupWindow.setFocusable(true);
        parent = findViewById(R.id.parent);
        popupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);


    }

    public void save() {

        editDes = customView.findViewById(R.id.edit_des);
        editTitle = customView.findViewById(R.id.edit_title);


        String title = editTitle.getText().toString();
        String des = editDes.getText().toString();

        if (title.isEmpty() || des.isEmpty()){
            Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_LONG).show();
        } else {

            popupWindow.dismiss();
            linear_layout = new LinearLayout(getApplicationContext());
            linear_layout.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(40, 20, 40, 20);
            linear_layout.setLayoutParams(params);
            linear_layout.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            linear_layout.setPadding(30, 30, 30, 30);


            txtDes = new TextView(getApplicationContext());
            txtTitle = new TextView(getApplicationContext());

            txtTitle.setTextSize(22);
            txtTitle.setTextColor(getResources().getColor(R.color.colorBlack));
            txtTitle.setMaxLines(1);
            txtTitle.setEllipsize(TextUtils.TruncateAt.END);

            txtDes.setTextSize(17);
            txtDes.setMaxLines(1);
            txtDes.setEllipsize(TextUtils.TruncateAt.END);

            txtDes.setText(des);
            txtTitle.setText(title);

            linear_layout.addView(txtTitle);
            linear_layout.addView(txtDes);

            parent.addView(linear_layout);
        }
    }

}
