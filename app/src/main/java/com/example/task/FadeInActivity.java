package com.example.task;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

public class FadeInActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fade_in);


        imageView = findViewById(R.id.fade_in_img);

        AlphaAnimation animation = new AlphaAnimation(0,1);
        animation.setDuration(4000);
        imageView.startAnimation(animation);
    }
}
