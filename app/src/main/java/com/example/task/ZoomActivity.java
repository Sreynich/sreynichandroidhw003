package com.example.task;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

public class ZoomActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        imageView = findViewById(R.id.zoom_img);

        ScaleAnimation animation = new ScaleAnimation( 0, 1,  0,1);
        animation.setDuration(4000);
        imageView.startAnimation(animation);
    }
}
